SERVER_MODE = False
LOG_FILE = '/opt/pgAdmin4/.pgadmin/pgadmin4.log'
SQLITE_PATH = '/opt/pgAdmin4/.pgadmin/pgadmin4.db'
SESSION_DB_PATH = '/opt/pgAdmin4/.pgadmin/sessions'
STORAGE_DIR = '/opt/pgAdmin4/.pgadmin/storage'
