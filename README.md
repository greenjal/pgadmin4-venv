# pgAdmin4 venv

Arch Linux has removed support for pgAdmin4. This is mainly done as arch linux has latest version of python and its dependencies. But pgAdmin4 may require some older version of dependencies. So it is really difficult to maintain pgAdmin4 on arch linux & its distros.

This AUR package tries to solve the issues by creating a virtual environment and installing pgAdmin4 from whl file in the virtual environment. This means pgAdmin4 uses all the dependencies from virtual environment & not system. So there will be less issues caused by different version of dependencies.

Since virtual environment can only be created on the system itself, the actual creation and installation of virtual environment & pgAdmin4 whl file will be done by post_install script.

## Known Issues

* ***If you are upgrading pgAdmin4 from arch repository, then configuration for pgadmin4 including database configuration will be lost. You can try to copy and replace the old configuration to /opt/pgAdmin4/.pgadmin/ and change owner to root but there were few issues for me.***
* Port 5050 will be used by pgAdmin4 service.
* All users will have to share the pgAdmin4 configurations as of now.
* There may be issues when python version upgrades.

## Install/Upgrade package

```bash
makepkg -sfi
```
